#include "Utils.h"
using namespace std;

// A Utility Function to check whether destination cell has
// been reached or not
bool isDestination(const Pair& position, const Pair& dest)
{
	return position == dest;
}

// A Utility Function to calculate the 'h' heuristics.
double calculateHValue(const Pair& src, const Pair& dest)
{
	// h is estimated with the two points distance formula
	return sqrt(pow((src.first - dest.first), 2.0)
		+ pow((src.second - dest.second), 2.0));
}

// GEORGE A Utility Function to calculate the Manhatten heuristics
double calculateManhattenHVal(const Pair& src, const Pair& dest)
{
	return (double)abs(src.first - dest.first) + (double)abs(src.second - dest.second);
}

// GEORGE A Utility Function to calculate the Diagonal heuristics
double calculateDiagonalHVal(const Pair& src, const Pair& dest)
{
	const double dx = abs(src.first - dest.first);
	const double dy = abs(src.second - dest.second);
	const double D = 1.0;
	const double D2 = sqrt(2);

	return D * (dx + dy) + (D2 - 2 * D) * min(dx, dy);
}

int incrementCellSuccessorVal(int& intRef)
{
	//if (intRef < 1)
	//{
		intRef++;
	//}
	//else
	//{
	//	intRef = -1;
	//}

	return intRef;


	//return intRef++ >= 1 ? intRef++ : intRef = -1;
}