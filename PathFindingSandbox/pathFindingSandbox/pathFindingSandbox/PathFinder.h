#pragma once
#include "Utils.h"
using namespace std;

template <typename T, size_t COL, size_t ROW>
class PathFinder
{
private:
	int i, j;
	Pair src, dest, currentCell, cellInvestigating;
	array<array<T, COL>, ROW> grid;
	std::vector<Pair> cellsInvestigating;
	bool destinationFound = false;

	// Create a closed list and initialise it to false which
	// means that no cell has been included yet This closed
	// list is implemented as a boolean 2D array
	bool closedList[ROW][COL];

	array<array<cell, COL>, ROW> cellDetails;

	/*
		Create an open list having information as-
		<f, <i, j>>
		where f = g + h,
		and i, j are the ROW and COLumn index of that cell
		Note that 0 <= i <= ROW-1 & 0 <= j <= COL-1
		This open list is implemented as a set of tuple.*/
	std::priority_queue<Tuple, std::vector<Tuple>, std::greater<Tuple> > openList;

public:
	PathFinder(const Pair& src, const Pair& dest);

	PathFinder(const Pair& src, const Pair& dest, array<array<T, COL>, ROW> grid);

	PathFinder(const Pair& src, const Pair& dest, char c);

	~PathFinder();

	const Pair getCurrentCell() const;

	const Pair getCellInvestigating() const;

	const std::vector<Pair> getCellsInvestigating() const;

	const auto getClosedList() const;
	const auto getCellDetails() const;
	const auto getOpenList() const;
	const bool isOpenListEmpty() const;
	const bool isDestinationFound() const;


	void getVertexAndUpdateLists();
	void popOpenList();
	void removeVertexFromClosedList(int i, int j);

	auto getGrid() const;

	// contained aStar search function
	void aStarSearch(const Pair& src, const Pair& dest);

	// aStar search function designed to be called every frame 
	void aStarSearchIterative(const int& cellSuccessorX, const int& cellSuccessorY);

	void setup();

};

template<typename T, size_t COL, size_t ROW>
inline void PathFinder<T, COL, ROW>::popOpenList()
{
	openList.pop();
}

template<typename T, size_t COL, size_t ROW>
inline void PathFinder<T, COL, ROW>::removeVertexFromClosedList(int i, int j)
{
	closedList[i][j] = true;
}

template<typename T, size_t COL, size_t ROW>
inline void PathFinder<T, COL, ROW>::setup()
{

	// PathFinder.src and PathFinder.dest must have been assigned for this function to be called


	// Initialising the parameters of the starting node
	i = src.first, j = src.second;
	cellDetails[i][j].f = 0.0;
	cellDetails[i][j].g = 0.0;
	cellDetails[i][j].h = 0.0;
	cellDetails[i][j].parent = { i, j };

	/* Create an open list having information as-
	<f, <i, j>>
	where f = g + h,
	and i, j are the ROW and COLumn index of that cell
	Note that 0 <= i <= ROW-1 & 0 <= j <= COL-1
	This open list is implemented as a set of tuple.*/
	//std::priority_queue<Tuple, std::vector<Tuple>, std::greater<Tuple> > openList;

	// Put the starting cell on the open list and set its
	// 'f' as 0
	openList.emplace(0.0, i, j);
}

template<typename T, size_t COL, size_t ROW>
inline PathFinder<T, COL, ROW>::PathFinder(const Pair& src, const Pair& dest)
{
	memset(closedList, false, sizeof(closedList));
	this->src = src;
	this->dest = dest;

	assert(verifyClassIntegrity(grid, src, dest));

}

template<typename T, size_t COL, size_t ROW>
inline PathFinder<T, COL, ROW>::PathFinder(const Pair& src, const Pair& dest, array<array<T, COL>, ROW> grid)
{
	memset(closedList, false, sizeof(closedList));
	this->src = src;
	this->dest = dest;
	this->grid = grid;

	assert(verifyClassIntegrity(grid, src, dest));
}

template<typename T, size_t COL, size_t ROW>
inline PathFinder<T, COL, ROW>::PathFinder(const Pair& src, const Pair& dest, char c)
{
	memset(closedList, false, sizeof(closedList));
	this->src = src;
	this->dest = dest;

	assert(verifyClassIntegrity(grid, src, dest));

}

template<typename T, size_t COL, size_t ROW>
inline PathFinder<T, COL, ROW>::~PathFinder()
{

}


// A Function to find the shortest path between a given
	// source cell to a destination cell according to A* Search
	// Algorithm
template<typename T, size_t COL, size_t ROW>
inline void PathFinder<T, COL, ROW>::aStarSearch(const Pair& src, const Pair& dest)
{
#pragma region setupFunctionalityNowEncapsulated
	/*
	//// If the source is out of range
	//if (!isValid(grid, src)) {
	//	printf("Source is invalid\n");
	//	return;
	//}
	//// If the destination is out of range
	//if (!isValid(grid, dest)) {
	//	printf("Destination is invalid\n");
	//	return;
	//}
	//// Either the source or the destination is blocked
	//if (!isUnBlocked(grid, src)
	//	|| !isUnBlocked(grid, dest)) {
	//	printf("Source or the destination is blocked\n");
	//	return;
	//}
	//// If the destination cell is the same as source cell
	//if (isDestination(src, dest)) {
	//	printf("We are already at the destination\n");
	//	return;
	//}
	// Create a closed list and initialise it to false which
	 //means that no cell has been included yet This closed
	 //list is implemented as a boolean 2D array
	//bool closedList[ROW][COL];
	//memset(closedList, false, sizeof(closedList));
	// Declare a 2D array of structure to hold the details
	// of that cell
	//array<array<cell, COL>, ROW> cellDetails;
	//int i, j;
	// Initialising the parameters of the starting node
	//i = src.first, j = src.second;
	//cellDetails[i][j].f = 0.0;
	//cellDetails[i][j].g = 0.0;
	//cellDetails[i][j].h = 0.0;
	//cellDetails[i][j].parent = { i, j };
	
	Create an open list having information as-
	<f, <i, j>>
	where f = g + h,
	and i, j are the ROW and COLumn index of that cell
	Note that 0 <= i <= ROW-1 & 0 <= j <= COL-1
	This open list is implemented as a set of tuple.
	//std::priority_queue<Tuple, std::vector<Tuple>, std::greater<Tuple> > openList;
	// Put the starting cell on the open list and set its
	// 'f' as 0
	//openList.emplace(0.0, i, j);*/
#pragma endregion

	// We set this boolean value as false as initially
	// the destination is not reached.
	while (!openList.empty())
	{
		const Tuple& p = openList.top();
		// Add this vertex to the closed list
		i = get<1>(p); // second element of tupla
		j = get<2>(p); // third element of tupla

		// Remove this vertex from the open list
		openList.pop();
		closedList[i][j] = true;
		/*
				Generating all the 8 successor of this cell
						N.W N N.E
						\ | /
						\ | /
						W----Cell----E
								/ | \
						/ | \
						S.W S S.E

				Cell-->Popped Cell (i, j)
				N --> North	 (i-1, j)
				S --> South	 (i+1, j)
				E --> East	 (i, j+1)
				W --> West		 (i, j-1)
				N.E--> North-East (i-1, j+1)
				N.W--> North-West (i-1, j-1)
				S.E--> South-East (i+1, j+1)
				S.W--> South-West (i+1, j-1)
		*/
		for (int add_x = -1; add_x <= 1; add_x++) {
			for (int add_y = -1; add_y <= 1; add_y++) {
				Pair neighbour(i + add_x, j + add_y);
				// Only process this cell if this is a valid
				// one
				if (isValid(grid, neighbour)) {
					// If the destination cell is the same
					// as the current successor
					if (isDestination(neighbour, dest))
					{ // Set the Parent of the destination cell
						cellDetails[neighbour.first][neighbour.second].parent = { i, j };
						printf("The destination cell is found\n");
						//tracePath(cellDetails, dest);
						destinationFound = true;
						return;
					}
					// If the successor is already on the
					// closed list or if it is blocked, then
					// ignore it. Else do the following
					else if (!closedList[neighbour.first][neighbour.second]
						&& isUnBlocked(grid, neighbour))
					{
						double gNew, hNew, fNew;
						gNew = cellDetails[i][j].g + 1.0; // TODO: GEORGE THIS IS WHERE WE NEED TO IMPLEMENT THE COST CALCULATION
						hNew = calculateHValue(neighbour, dest);
						fNew = gNew + hNew;

						// If it isn�t on the open list, add
						// it to the open list. Make the
						// current square the parent of this
						// square. Record the f, g, and h
						// costs of the square cell
						//			 OR
						// If it is on the open list
						// already, check to see if this
						// path to that square is better,
						// using 'f' cost as the measure.
						if (cellDetails[neighbour.first][neighbour.second].f == -1
							|| cellDetails[neighbour.first][neighbour.second].f > fNew)
						{
							openList.emplace(fNew, neighbour.first, neighbour.second);

							// Update the details of this
							// cell
							cellDetails[neighbour.first][neighbour.second].g = gNew;
							cellDetails[neighbour.first][neighbour.second].h = hNew;
							cellDetails[neighbour.first][neighbour.second].f = fNew;
							cellDetails[neighbour.first][neighbour.second].parent = { i, j };
						}
					}
				}
			}
		}
	}

	// When the destination cell is not found and the open
	// list is empty, then we conclude that we failed to
	// reach the destiantion cell. This may happen when the
	// there is no way to destination cell (due to
	// blockages)
	printf("Failed to find the Destination Cell\n");
}

// A Function to find the shortest path between a given
// source cell to a destination cell according to A* Search
// Algorithm
template<typename T, size_t COL, size_t ROW>
inline void PathFinder<T, COL, ROW>::aStarSearchIterative(const int& cellSuccessorX, const int& cellSuccessorY)
{
	/*while (!openList.empty())
	//{
		//const Tuple& p = openList.top();
		//// Add this vertex to the closed list
		//i = get<1>(p); // second element of tupla
		//j = get<2>(p); // third element of tupla
		//// Remove this vertex from the open list
		//openList.pop();
		//closedList[i][j] = true;

				Generating all the 8 successor of this cell
					N.W	  N		N.E
					   \  |    /
						\ |   /
					W----Cell----E
					   /  |	 \
					  /   |	  \
					S.W   S	  S.E

				Cell-->Popped Cell (i, j)
				N --> North	 (i-1, j)
				S --> South	 (i+1, j)
				E --> East	 (i, j+1)
				W --> West		 (i, j-1)
				N.E--> North-East (i-1, j+1)
				N.W--> North-West (i-1, j-1)
				S.E--> South-East (i+1, j+1)
				S.W--> South-West (i+1, j-1)

		//for (int add_x = -1; add_x <= 1; add_x++) {
			//for (int add_y = -1; add_y <= 1; add_y++) {*/
	Pair neighbour(i + cellSuccessorX, j + cellSuccessorY);
	cellInvestigating = neighbour;
	currentCell = make_pair(i, j);
	// Only process this cell if this is a valid
	// one
	if (isValid(grid, neighbour)) {
		// If the destination cell is the same
		// as the current successor
		if (isDestination(neighbour, dest))
		{ // Set the Parent of the destination cell
			cellDetails[neighbour.first][neighbour.second].parent = { i, j };
			printf("The destination cell is found\n");
			//tracePath(cellDetails, dest);
			destinationFound = true;
			//openList.pop();
			return;
		}
		// If the successor is already on the
		// closed list or if it is blocked, then
		// ignore it. Else do the following
		else if (!closedList[neighbour.first][neighbour.second]
			&& isUnBlocked(grid, neighbour))
		{
			double gNew, hNew, fNew;
			gNew = cellDetails[i][j].g + 1.0;
			hNew = calculateHValue(neighbour, dest);
			fNew = gNew + hNew;

			// If it isn�t on the open list, add
			// it to the open list. Make the
			// current square the parent of this
			// square. Record the f, g, and h
			// costs of the square cell
			//			 OR
			// If it is on the open list
			// already, check to see if this
			// path to that square is better,
			// using 'f' cost as the measure.
			if (cellDetails[neighbour.first][neighbour.second].f == -1
				|| cellDetails[neighbour.first][neighbour.second].f > fNew)
			{
				openList.emplace(fNew, neighbour.first, neighbour.second);

				// Update the details of this
				// cell
				cellDetails[neighbour.first][neighbour.second].g = gNew;
				cellDetails[neighbour.first][neighbour.second].h = hNew;
				cellDetails[neighbour.first][neighbour.second].f = fNew;
				cellDetails[neighbour.first][neighbour.second].parent = { i, j };
			}
		}
	}
	/*//}
//}
//}

// When the destination cell is not found and the open
// list is empty, then we conclude that we failed to
// reach the destiantion cell. This may happen when the
// there is no way to destination cell (due to
// blockages)
//printf("Failed to find the Destination Cell\n");*/
}


template<typename T, size_t COL, size_t ROW>
inline const Pair PathFinder<T, COL, ROW>::getCurrentCell() const
{
	return currentCell;
}

template<typename T, size_t COL, size_t ROW>
inline const Pair PathFinder<T, COL, ROW>::getCellInvestigating() const
{
	return cellInvestigating;
}

template<typename T, size_t COL, size_t ROW>
inline const std::vector<Pair> PathFinder<T, COL, ROW>::getCellsInvestigating() const
{
	return cellsInvestigating;
}

template<typename T, size_t COL, size_t ROW>
inline auto PathFinder<T, COL, ROW>::getGrid() const
{
	return grid;
}

template<typename T, size_t COL, size_t ROW>
inline const auto PathFinder<T, COL, ROW>::getClosedList() const
{
	return closedList;
}

template<typename T, size_t COL, size_t ROW>
inline const auto PathFinder<T, COL, ROW>::getCellDetails() const
{
	return cellDetails;
}

template<typename T, size_t COL, size_t ROW>
inline const auto PathFinder<T, COL, ROW>::getOpenList() const
{
	return openList;
}

template<typename T, size_t COL, size_t ROW>
inline const bool PathFinder<T, COL, ROW>::isOpenListEmpty() const
{
	return openList.empty();
}

template<typename T, size_t COL, size_t ROW>
inline void PathFinder<T, COL, ROW>::getVertexAndUpdateLists()
{
	//const Tuple& p = getOpenList().top();
	const Tuple& p = openList.top();

	// Add this vertex to the closed list
	i = get<1>(p); // second element of tupla
	j = get<2>(p); // third element of tupla

	// Remove this vertex from the open list
	popOpenList();
	removeVertexFromClosedList(i, j);
}

template<typename T, size_t COL, size_t ROW>
inline const bool PathFinder<T, COL, ROW>::isDestinationFound() const
{
	return destinationFound;
}