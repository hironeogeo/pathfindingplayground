#include <iostream>
#include <conio.h>
#include <chrono>
#include <thread>
#include <array>

#include "PathFinder.h"
#include "Utils.h"

using namespace std;

constexpr char currentCellMarker = 'c';
constexpr char cellInvestigatingMarker = 'i';
constexpr char startCellMarker = 'S';
constexpr char destinationCellMarker = 'D';
constexpr char gridVSideWall = '|';
constexpr int cellProgressionLimit = 2;
constexpr double sleepVal = 1000.0; // ms to sleep before next update
constexpr bool useIncremental = true;

int cellSuccessionX = -1;
int cellSuccessionY = -1;

Pair start = { 8,0 };
Pair dest = { 0,0 };

//	1--> The cell is not blocked
//	0--> The cell is blocked */
array<array<int, 10>, 9> grid
{
	{
		{ { 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 } },
		{ { 1, 1, 1, 0, 1, 1, 1, 0, 1, 1 } },
		{ { 1, 1, 1, 0, 1, 1, 0, 1, 0, 1 } },
		{ { 0, 0, 1, 0, 1, 0, 0, 0, 0, 1 } },
		{ { 1, 1, 1, 0, 1, 1, 1, 0, 1, 0 } },
		{ { 1, 0, 1, 1, 1, 1, 0, 1, 0, 0 } },
		{ { 1, 0, 0, 0, 0, 1, 0, 0, 0, 1 } },
		{ { 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 } },
		{ { 1, 1, 1, 0, 0, 0, 1, 0, 0, 1 } }
	}
};

double PCFreq = 0.0;
__int64 CounterStart = 0;

std::chrono::system_clock::time_point a = std::chrono::system_clock::now();
std::chrono::system_clock::time_point b = std::chrono::system_clock::now();

template<typename T, size_t COL, size_t ROW>
void update(PathFinder<T, COL, ROW>& pathfinder);

template<typename T, size_t COL, size_t ROW>
void render(PathFinder<T, COL, ROW>& pathfinder);

int main()
{
	/* Notes
	for the pathfinding incremental loop we split the logic into these repetable steps

	1) take a starting cell and a destination
	2) do some logic to investigate the surrounding cells
	3) decide which if any of the surrounding cells are viable options
	4) start again at 1) with an updated starting cell
	
	for the load balancing maps we could run a simple comparison of the routes generated 
	and if there are any nodes / cells the same then those cells will can be load balancers 
	and can determine which route to send the enemies on when they arrive
	
	*/

	PathFinder<int, 10, 9> pathFinder(start, dest, grid);
	pathFinder.setup();

	if (useIncremental)
	{
		while (!pathFinder.isOpenListEmpty() && !pathFinder.isDestinationFound())
		{
			// Maintain time between frames
			a = std::chrono::system_clock::now();
			std::chrono::duration<double, std::milli> work_time = a - b;

			if (work_time.count() < sleepVal)
			{
				std::chrono::duration<double, std::milli> delta_ms(sleepVal - work_time.count());
				auto delta_ms_duration = std::chrono::duration_cast<std::chrono::milliseconds>(delta_ms);
				std::this_thread::sleep_for(std::chrono::milliseconds(delta_ms_duration.count()));
			}

			b = std::chrono::system_clock::now();

			// if navigation to target cell possible
			// perform 1 iteration
			// else we should finish

			update(pathFinder);
			render(pathFinder);
		}
	}
	else
	{
		pathFinder.aStarSearch(start, dest);
	}

	cout << endl << endl;
	if (pathFinder.isDestinationFound())
	{
		cout << "destination Found" << endl;
		tracePath(pathFinder.getCellDetails(), dest);
		cout << endl;
	}
	else
	{
		cout << "destination not found" << endl << endl;
	}
	system("pause");
	return 0;
}

// function we should call every frame to update the logic
template<typename T, size_t COL, size_t ROW>
void update(PathFinder<T, COL, ROW>& pathfinder)
{
	if (cellSuccessionX < cellProgressionLimit)
	{
		if (cellSuccessionY <= cellProgressionLimit)
		{
			pathfinder.aStarSearchIterative(cellSuccessionX, cellSuccessionY);
			incrementCellSuccessorVal(cellSuccessionY);

			if (cellSuccessionY >= cellProgressionLimit)
			{
				incrementCellSuccessorVal(cellSuccessionX);
				cellSuccessionY = -1;
			}
		}
	}
	else
	{
		cellSuccessionX = -1;
		cellSuccessionY = -1;
		pathfinder.getVertexAndUpdateLists();
	}
}

// function we should call every frame to draw the grid on the screen
template<typename T, size_t COL, size_t ROW>
void render(PathFinder<T, COL, ROW>& pathfinder)
{
	void drawGrid(Pair cellInvestigating, Pair currentCell);
	// clear the screen
	system("CLS");
	drawGrid(pathfinder.getCellInvestigating(), pathfinder.getCurrentCell());
}

void drawGrid(Pair cellInvestigating, Pair currentCell)
{
	const int ROWs = grid.size();
	const int COLUMNS = grid[0].size();

	for (int i = 0; i < ROWs; ++i)
	{
		for (int j = 0; j < COLUMNS; ++j)
		{
			cout << gridVSideWall;

			if (j == start.second && i == start.first)
			{
				cout << startCellMarker;
			}
			else if (j == dest.second && i == dest.first)
			{
				cout << destinationCellMarker;
			}
			else if (j == cellInvestigating.second && i == cellInvestigating.first)
			{
				cout << cellInvestigatingMarker;
			}
			else if (j == currentCell.second && i == currentCell.first)
			{
				cout << currentCellMarker;
			}
			else
			{
				cout << grid[i][j];
			}
		}
		cout << gridVSideWall << endl;
	}
}