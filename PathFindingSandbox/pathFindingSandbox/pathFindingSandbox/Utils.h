#pragma once
#include <array>
#include <tuple>
#include <queue>
#include <set>
#include <stack>
#include <assert.h>

// Creating a shortcut for int, int pair type
typedef std::pair<int, int> Pair;
// Creating a shortcut for tuple<int, int, int> type
typedef std::tuple<double, int, int> Tuple;

bool isDestination(const Pair& position, const Pair& dest);
double calculateHValue(const Pair& src, const Pair& dest);
double calculateManhattenHVal(const Pair& src, const Pair& dest);
double calculateDiagonalHVal(const Pair& src, const Pair& dest);
int incrementCellSuccessorVal(int& intRef);

// A structure to hold the necessary parameters
struct cell {
	// ROW and COLumn index of its parent
	Pair parent;
	// f = g + h
	double f, g, h;
	cell()
		: parent(-1, -1)
		, f(-1)
		, g(-1)
		, h(-1)
	{
	}
};

// A Utility Function to check whether given cell (ROW, COL)
// is a valid cell or not.
template <typename T, size_t COL, size_t ROW>
bool isValid(const std::array<std::array<T, COL>, ROW>& grid,
	const Pair& point)
{ // Returns true if ROW number and COLumn number is in
// range
	if (ROW > 0 && COL > 0)
		return (point.first >= 0) && (point.first < ROW)
		&& (point.second >= 0)
		&& (point.second < COL);

	return false;
}

// A Utility Function to check whether the given cell is
// blocked or not
template <typename T, size_t COL, size_t ROW>
bool isUnBlocked(const std::array<std::array<T, COL>, ROW>& grid,
	const Pair& point)
{
	// Returns true if the cell is not blocked else false
	return isValid(grid, point)
		&& grid[point.first][point.second] == 1;
}

// A Utility Function to trace the path from the source to
// destination
template <size_t ROW, size_t COL>
void tracePath(
	const std::array<std::array<cell, COL>, ROW>& cellDetails,
	const Pair& dest)
{
	printf("\nThe Path is ");

	std::stack<Pair> path;
	path.push(dest);

	int row = dest.first;
	int col = dest.second;
	Pair next_node;
	do {
		next_node = cellDetails[row][col].parent;
		row = next_node.first;
		col = next_node.second;
		path.push(next_node);
	} while (cellDetails[row][col].parent != next_node);

	while (!path.empty()) {
		Pair p = path.top();
		path.pop();
		printf("-> (%d,%d) ", p.first, p.second);
	}
}

// A Utility Function to verify class has been initialised correctly
template <typename T, size_t COL, size_t ROW>
bool verifyClassIntegrity(std::array<std::array<T, COL>, ROW> grid, const Pair& src, const Pair& dest)
{
	// If the source is out of range
	if (!isValid(grid, src)) {
		printf("Source is invalid\n");
		return false;
	}

	// If the destination is out of range
	if (!isValid(grid, dest)) {
		printf("Destination is invalid\n");
		return false;
	}

	// Either the source or the destination is blocked
	if (!isUnBlocked(grid, src)
		|| !isUnBlocked(grid, dest)) {
		printf("Source or the destination is blocked\n");
		return false;
	}

	// If the destination cell is the same as source cell
	if (isDestination(src, dest)) {
		printf("We are already at the destination\n");
		return false;
	}

	return true;
}

