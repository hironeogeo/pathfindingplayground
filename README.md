# Pathfinding Playground

This is a console based tool I built to develop, test and debug issues with the pathfinding functionality used in [Plants vs Vegans](https://gitlab.com/hironeogeo/plants-vs-vegans)

Initially the Pathfinding functionality took a start and end position and then returned a route or not. This wasn't great for development and testing so I built an incrementatl version that could be called each frame.

I then build a very simple game loop with an update function to call the new incremental version each frame and a render function which would draw a representation of the grid and cells being investigated.

I then created a throttle to restict the time for each frame so that the speed of the increments could be slowed down - in this case to 1 iteration per second.

This allowed the algorithm to be tested to make sure that it was working but then also to debug issues when they came to light by being able to reproduce them with this tool and follow the functionality step by step.

This tool isn't very pretty as it's just a console application but it was useful. It could be improved with something like ImGUI or Qt to create a prettier output on a revisit.